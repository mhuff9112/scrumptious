from django.shortcuts import render, get_object_or_404, render, redirect
from recipes.models import Recipe
from recipes.templates.recipes.forms import RecipeForm

def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": recipe
    }
    return render(request, "recipes/details.html", context)

def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes
    }
    return render(request, "recipes/list.html", context)

def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            form.save()
            return redirect("recipe_list")
    else:
        form = RecipeForm()
        
    context = {
        "form": form,
    }
    return render(request, "recipes/create.html", context)

def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=id)
    else:
        form = RecipeForm(instance=recipe)
        
    context = {
        "recipe_object": recipe,
        "form": form,
        
    }
    return render(request, "recipes/edit.html", context)
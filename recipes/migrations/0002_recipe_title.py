# Generated by Django 4.1.5 on 2023-01-11 23:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='recipe',
            name='title',
            field=models.CharField(default='temp', max_length=200),
            preserve_default=False,
        ),
    ]
